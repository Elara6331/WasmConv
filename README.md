# WasmConv
Website that converts strings to and from various encodings. Written in [Go](https://golang.org) using [WebAssembly](https://webassembly.org) and [Vugu](https://vugu.org).

I created this project to learn how WebAssembly is implemented and functions.

Start development server using:

```sh
go run . -dev
```

To build release, install [AdvMake](https://gitea.arsenm.dev/Arsen6331/advmake), then use:

```sh
advmake dist
```
To start the server, enter the `dist` directory and run:

```bash
./server-linux-$(uname -m)
```

Then browse to the running server: http://localhost:8844/
