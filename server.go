// +build !wasm

package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/vugu/vugu/devutil"
)

func main() {
	dev := flag.Bool("dev", false, "Enable development features")
	dir := flag.String("dir", ".", "Project directory")
	httpl := flag.String("addr", ":8844", "Listen for HTTP on this host:port")
	flag.Parse()

	wd, _ := filepath.Abs(*dir)
	os.Chdir(wd)

	mux := devutil.NewMux()
	mux.Match(devutil.NoFileExt, devutil.DefaultIndex.Replace(
		`<!-- styles -->`,
		`<link rel="stylesheet" href="/css/bulma.min.css">`,
	).Replace(
		`<!-- scripts -->`,
		`<script src="/js/iconify.min.js"></script>`,
	).Replace(
		`<title>Vugu App</title>`,
		`<title>WasmConv</title>`,
	))
	mux.Default(devutil.NewFileServer().SetDir("."))

	if *dev {
		wc := devutil.NewWasmCompiler().SetDir(".")
		mux.Exact("/main.wasm", devutil.NewMainWasmHandler(wc))
		mux.Exact("/wasm_exec.js", devutil.NewWasmExecJSHandler(wc))
		log.Printf("Starting HTTP Server at %q", *httpl)
		log.Fatal(http.ListenAndServe(*httpl, mux))
	} else {
		log.Printf("Starting HTTP Server at %q", *httpl)
		log.Fatal(http.ListenAndServe(*httpl, mux))
	}
}
